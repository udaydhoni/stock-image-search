let bodyToAppend = document.querySelector('.images');
let accessKey = '8iV_ej9bM6WMVmPTBsGkvNCtid71LnlgB0ER9u2v8PE';
let randomURL = `https://api.unsplash.com/photos/random?client_id=${accessKey}&count=30`;
let query = document.querySelector('.query');
let searchTerm = '';
let searchURL = `https://api.unsplash.com/photos/random?client_id=${accessKey}&query=${searchTerm}&count=30`;
let searchButton = document.querySelector('.search');



function UI (url = randomURL,searchValue='') {
    if (searchValue) {
        searchTerm = searchValue;
        searchURL = `https://api.unsplash.com/photos/random?client_id=${accessKey}&query=${searchTerm}&count=30`;
        url = searchURL;
    } 
    fetch(url).then(obj => obj.json()).then(obj => {obj.forEach((elem)=>{
        let divImage =document.createElement('div');
        divImage.classList.add('img');
        divImage.style.backgroundImage = `url(${elem['urls']['regular']})`;
        divImage.style.width = '400px';
        divImage.style.height= '250px';
        divImage.style.background.repeat = 'no-repeat';
        divImage.style.backgroundSize = '400px 250px';
        let detailsDiv = document.createElement('div');
        let username = document.createElement('p');
        username.innerText = elem['user']['name']
        let likes = document.createElement('p');
        let profileimage = document.createElement('img');
        profileimage.src= elem['user']['profile_image']['medium'];
        likes.innerText = elem['likes'] + ' '+'likes';
        detailsDiv.classList.add('details');
        detailsDiv.append(profileimage);
        detailsDiv.append(username);
        detailsDiv.append(likes);
        divImage.append(detailsDiv)
    
        detailsDiv.style.display = 'none';
        bodyToAppend.append(divImage);
    })})
}


// function randomUI() {
//     bodyToAppend.innerHTML = '';
//     UI(randomURL);
//     }

// function searchUI (url) {
//     bodyToAppend.innerHTML = '';
//     UI(url);
// }

if (searchTerm == '') {
    UI(randomURL);
}


searchButton.addEventListener('click',()=>{
    bodyToAppend.innerHTML = '';
    UI(randomURL,query.value);
})


query.addEventListener('keyup',(event)=>{
        if (query.value == '') {
            bodyToAppend.innerHTML= '';
            UI(randomURL,'');
        }
        else if (event.key == 'Enter') {
            bodyToAppend.innerHTML= '';
            UI(randomURL,query.value);
        }
    
    
})

// window.addEventListener('click', ()=>{
//     let count =1;
//     // if (document.body.scrollTop > 834) {
//         if (query.value != '') {
//             searchTerm = query.value;
//             let searchURL = `https://api.unsplash.com/photos/random?page=${count}client_id=${accessKey}&query=${searchTerm}&count=30`;
//             searchUI(searchURL);
//             console.log
    
//         }
//     // }
// })

bodyToAppend.addEventListener('mouseover',(event)=>{
    if (event.target.classList.contains('img')) {
        event.target.children[0].style.display = 'block';
    }
     else if (event.target.classList.contains('details')) {
        event.target.style.display = 'block';
    } 
})

bodyToAppend.addEventListener('mouseout',(event)=>{
    if (event.target.classList.contains('img')) {
        event.target.children[0].style.display = 'none';
    }
})

